// Class includes
#include "Systems/PlayerSystem.hpp"

// ECS includes
#include "ECS/Entity.hpp"

// Own includes
#include "Components/PlayerComponent.hpp"

PlayerSystem::PlayerSystem(ecs::EntityManager &entityManager) :
ecs::System(entityManager)
{
    require<PlayerComponent>();
}

void PlayerSystem::onUpdate(const std::vector<ecs::Entity*> &entities, float dt) {
    for(auto *entity : entities) {
        PlayerComponent *player = entity->getComponent<PlayerComponent>();

        // Input::handleControls(controller, player);
    }
}
