// Class includes
#include "Systems/MovementSystem.hpp"

// ECS includes
#include "ECS/Entity.hpp"

// Own includes
#include "Components/TransformComponent.hpp"
#include "Components/VelocityComponent.hpp"

MovementSystem::MovementSystem(ecs::EntityManager &entityManager) :
ecs::System(entityManager)
{
    require<TransformComponent>();
    require<VelocityComponent>();
}

void MovementSystem::onUpdate(const std::vector<ecs::Entity*> &entities, float dt)
{
    for(auto *entity : entities) {
        TransformComponent *transform = entity->getComponent<TransformComponent>();
        VelocityComponent *velocity = entity->getComponent<VelocityComponent>();

        velocity->vX += transform->x * dt;
        velocity->vY += transform->y * dt;
    }
}
