// Class includes
#include "Clock.hpp"

// STL includes
#include <chrono>

const double Clock::MILLISECONDS_DIVISOR = 1000000.0;
const double Clock::SECONDS_DIVISOR = 1000.0;

Clock::Clock() :
startTime(now())
{
}

unsigned long long Clock::getElapsed()
{
    return now() - startTime;
}

double Clock::getElapsedMilliseconds()
{
    return static_cast<double>(getElapsed()) / MILLISECONDS_DIVISOR;
}

double Clock::getElapsedSeconds()
{
    return getElapsedMilliseconds() / SECONDS_DIVISOR;
}

unsigned long long Clock::reset()
{
    auto elapsed = getElapsed();
    startTime = now();

    return elapsed;
}

double Clock::resetMilliseconds()
{
    return static_cast<double>(reset()) / MILLISECONDS_DIVISOR;
}

double Clock::resetSeconds()
{
    return resetMilliseconds() / SECONDS_DIVISOR;
}

unsigned long long Clock::now()
{
    std::chrono::high_resolution_clock::time_point timePointNow = std::chrono::high_resolution_clock::now();
    std::chrono::duration<unsigned long long, std::nano> timeSinceEpoch = timePointNow.time_since_epoch();

    return timeSinceEpoch.count();
}

double Clock::nowMilliseconds()
{
    return static_cast<double>(now()) / MILLISECONDS_DIVISOR;
}

double Clock::nowSeconds()
{
    return nowMilliseconds() / SECONDS_DIVISOR;
}
