// Class includes
#include "ECS/System.hpp"

namespace ecs {

    System::System(EntityManager& entityManager) :
    entityManager(entityManager)
    {
    }

    System::~System()
    {
    }

    void System::update(float dt)
    {
        onUpdate(entityManager.findByComponentTypeHashes(componentTypeHashes), dt);
    }

} // namespace ecs
