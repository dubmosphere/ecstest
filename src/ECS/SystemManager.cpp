// Class includes
#include "ECS/SystemManager.hpp"

// ECS includes
#include "ECS/System.hpp"

namespace ecs {

SystemManager::SystemManager(EntityManager& entityManager) :
entityManager(entityManager)
{
}

void SystemManager::update(float dt)
{
    std::for_each(std::begin(systems), std::end(systems), [&dt](auto& system) {
        system->update(dt);
    });
}

} // namespace ecs
