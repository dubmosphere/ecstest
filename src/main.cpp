// ECS includes
#include "ECS/ECS.hpp"

// Own includes
#include "Systems/MovementSystem.hpp"
#include "Systems/PlayerSystem.hpp"
#include "EntityFactory.hpp"
#include "Clock.hpp"

// STL includes
#include <iostream>

// Initialize the entity and system managers, populate the world with entities, add some systems and update everything once to see if it works
int main(int argc, char **argv) {
    ecs::EntityManager entityManager;
    ecs::SystemManager &systemManager = entityManager.getSystemManager();
    EntityFactory entityFactory(entityManager);

    systemManager.addSystem<MovementSystem>();
    systemManager.addSystem<PlayerSystem>();

    ecs::Entity *player = entityFactory.createPlayer();

    for(int i = 0; i < 20000; ++i) {
        entityFactory.createMovable();
    }

    Clock clock;
    double fps = 800.0;
    double timePerFrame = 1.0 / 60.0;
    double accumulator = 0.0;

    while(true) {
        while(accumulator >= timePerFrame) {
            entityManager.update(static_cast<float>(timePerFrame));

            std::cout << (timePerFrame / accumulator) * fps  << std::endl;
            accumulator -= timePerFrame;
        }

        accumulator += clock.resetSeconds();
    }

    return 0;
}
