// Class includes
#include "EntityFactory.hpp"

// ECS includes
#include "ECS/Entity.hpp"

// Own includes
#include "Components/TransformComponent.hpp"
#include "Components/VelocityComponent.hpp"
#include "Components/PlayerComponent.hpp"

EntityFactory::EntityFactory(ecs::EntityManager &entityManager) :
ecs::EntityFactory(entityManager)
{
}


ecs::Entity *EntityFactory::createMovable()
{
    ecs::Entity *entity = createEntity();

    entity->addComponent<TransformComponent>();
    entity->addComponent<VelocityComponent>();

    return entity;
}


ecs::Entity *EntityFactory::createPlayer()
{
    ecs::Entity *player = createMovable();
    player->addComponent<PlayerComponent>();

    return player;
}
