#ifndef ECS_COMPONENT_HPP
#define ECS_COMPONENT_HPP

namespace ecs {

/**
 * The Component struct is just an empty struct every Component should derive of.
 */
struct Component
{
    virtual ~Component() {};
};

} // namespace ecs

#endif // ECS_COMPONENT_HPP
