namespace ecs {

template <class Type>
const unsigned long long EntityManager::getTypeHash()
{
    return typeid(Type).hash_code();
}

} // namespace ecs
