namespace ecs {

template<class SystemType>
void SystemManager::addSystem()
{
    if(!hasSystem<SystemType>()) {
        systems.push_back(std::make_unique<SystemType>(entityManager));
    }
}

template<class SystemType>
const bool SystemManager::hasSystem() {
    for(auto& system : systems) {
        if(EntityManager::getTypeHash<SystemType>() == typeid(*system).hash_code()) {
            return true;
        }
    }

    return false;
}

} // namespace ecs
