#ifndef ECS_SYSTEM_HPP
#define ECS_SYSTEM_HPP

// ECS includes
#include "ECS/EntityManager.hpp"

// STL includes
#include <algorithm>

namespace ecs {

class System
{
    public:
        System(EntityManager& entityManager);
        virtual ~System();

        void update(float dt);

    protected:
        template<class ComponentType>
        void require();

        EntityManager& entityManager;

    private:
        virtual void onUpdate(const std::vector<Entity*>& entities, float dt) = 0;

        std::vector<unsigned long long> componentTypeHashes;
};

} // namespace ecs

#include "ECS/System.inl"

#endif // ECS_SYSTEM_HPP
