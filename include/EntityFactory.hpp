#ifndef ENTITYFACTORY_HPP
#define ENTITYFACTORY_HPP

#include "ECS/EntityFactory.hpp"

class EntityFactory : ecs::EntityFactory {
    public:
        EntityFactory(ecs::EntityManager &entityManager);

        ecs::Entity *createMovable();
        ecs::Entity *createPlayer();
};

#endif // ENTITYFACTORY_HPP
