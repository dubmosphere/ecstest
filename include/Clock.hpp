#ifndef CLOCK_HPP
#define CLOCK_HPP

class Clock {
    public:
        Clock();

        unsigned long long getElapsed();
        double getElapsedMilliseconds();
        double getElapsedSeconds();
        unsigned long long reset();
        double resetMilliseconds();
        double resetSeconds();

        static unsigned long long now();
        static double nowMilliseconds();
        static double nowSeconds();

    private:
        static const double MILLISECONDS_DIVISOR;
        static const double SECONDS_DIVISOR;

        unsigned long long startTime;
};

#endif // CLOCK_HPP
