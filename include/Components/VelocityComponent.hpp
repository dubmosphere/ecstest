#ifndef VELOCITYCOMPONENT_HPP
#define VELOCITYCOMPONENT_HPP

// ECS includes
#include "ECS/Component.hpp"

struct VelocityComponent : ecs::Component {
    float vX = 10;
    float vY = 10;
};

#endif // VELOCITYCOMPONENT_HPP
