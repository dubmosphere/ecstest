#ifndef TRANSFORMCOMPONENT_HPP
#define TRANSFORMCOMPONENT_HPP

// ECS includes
#include "ECS/Component.hpp"

struct TransformComponent : ecs::Component {
    float x = 100;
    float y = 100;
};

#endif // TRANSFORMCOMPONENT_HPP
