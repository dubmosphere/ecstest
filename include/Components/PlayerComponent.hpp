#ifndef PLAYERCOMPONENT_HPP
#define PLAYERCOMPONENT_HPP

// ECS includes
#include "ECS/Component.hpp"

struct PlayerComponent : ecs::Component {
    int number = 1;
};

#endif // PLAYERCOMPONENT_HPP
