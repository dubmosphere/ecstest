#ifndef MOVEMENTSYSTEM_HPP
#define MOVEMENTSYSTEM_HPP

// ECS includes
#include "ECS/System.hpp"

namespace ecs {
    class EntityManager;
    class Entity;
}

struct MovementSystem : ecs::System {
    MovementSystem(ecs::EntityManager &entityManager);

    void onUpdate(const std::vector<ecs::Entity*> &entities, float dt);
};

#endif // MOVEMENTSYSTEM_HPP
