#ifndef PLAYERSYSTEM_HPP
#define PLAYERSYSTEM_HPP

// ECS includes
#include "ECS/System.hpp"

namespace ecs {
    class EntityManager;
    class Entity;
}

struct PlayerSystem : ecs::System {
    PlayerSystem(ecs::EntityManager &entityManager);

    void onUpdate(const std::vector<ecs::Entity*> &entities, float dt);
};

#endif // PLAYERSYSTEM_HPP
