#ifndef LOG_HPP
#define LOG_HPP

#include <fstream>

class Log
{
    public:
        Log();
        ~Log();

        void write(const std::string &text);

    private:
        std::ofstream fp;
};

#endif // LOG_HPP
